package panov.test.springboot.graphql.repo;

import org.springframework.data.repository.CrudRepository;
import panov.test.springboot.graphql.domain.Book;

import java.util.Set;

public interface BookRepo extends CrudRepository<Book, Long> {
    Set<Book> getBooksByAuthorId(Long authorId);
}

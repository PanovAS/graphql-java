package panov.test.springboot.graphql.repo;

import org.springframework.data.repository.CrudRepository;
import panov.test.springboot.graphql.domain.Author;

public interface AuthorRepo extends CrudRepository<Author, Long> {
}

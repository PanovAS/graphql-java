package panov.test.springboot.graphql;

import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import io.leangen.graphql.GraphQLSchemaGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import panov.test.springboot.graphql.graphql.AuthorGraph;
import panov.test.springboot.graphql.graphql.BookGraph;

@SpringBootApplication
@EnableJpaRepositories
public class GraphqlApplication {
	//http://localhost:8080/graphiql
	public static void main(String[] args) {
		SpringApplication.run(GraphqlApplication.class, args);
	}

	@Bean
	public GraphQL graphQL(AuthorGraph authorGraph, BookGraph bookGraph) {
		GraphQLSchema schema = new GraphQLSchemaGenerator()
				.withBasePackages("panov.test.springboot.graphql")
				.withOperationsFromSingletons(authorGraph, bookGraph)
				.generate();
		return GraphQL.newGraphQL(schema).build();
	}
}

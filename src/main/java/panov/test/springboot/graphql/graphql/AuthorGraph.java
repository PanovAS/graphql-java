package panov.test.springboot.graphql.graphql;

import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import panov.test.springboot.graphql.domain.Author;
import panov.test.springboot.graphql.repo.AuthorRepo;

import java.util.Collections;

@Component
@RequiredArgsConstructor
public class AuthorGraph {
    private final AuthorRepo authorRepo;

    @GraphQLQuery(name = "author")
    public Author getAuthorById(@GraphQLArgument(name = "id") Long id) {
        return authorRepo.findById(id).orElse(null);
    }

    @GraphQLQuery(name = "authors")
    public Iterable<Author> getAllAuthors() {
        return authorRepo.findAll();
    }

    @GraphQLMutation(name = "createAuthor")
    public Author createAuthor(@GraphQLArgument(name = "id") Long id,
                               @GraphQLArgument(name = "firstName") String firstName,
                               @GraphQLArgument(name = "secondName") String secondName) {
        return authorRepo.save(new Author(id, firstName, secondName, Collections.emptySet()));
    }
}

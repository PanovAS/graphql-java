package panov.test.springboot.graphql.graphql;

import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import panov.test.springboot.graphql.domain.Book;
import panov.test.springboot.graphql.repo.BookRepo;

@Component
@RequiredArgsConstructor
public class BookGraph {
    private final BookRepo bookRepo;

    @GraphQLQuery(name = "books")
    public Iterable<Book> getAllBooks() {
        return bookRepo.findAll();
    }

    @GraphQLQuery(name = "books")
    public Iterable<Book> getBooksByAuthorId(@GraphQLArgument(name = "author_id") Long authorId) {
        return bookRepo.getBooksByAuthorId(authorId);
    }

    @GraphQLMutation(name = "createBook")
    public Book createBook(@GraphQLArgument(name = "id") Long id,
                           @GraphQLArgument(name = "name") String name,
                           @GraphQLArgument(name = "description") String description,
                           @GraphQLArgument(name = "author_id") Long authorId) {
        return bookRepo.save(new Book(id, name, description, authorId));
    }
}
